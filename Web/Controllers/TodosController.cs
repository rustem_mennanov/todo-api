﻿using Microsoft.AspNetCore.Mvc;
using System;
using Web.Services;

namespace Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TodosController : ControllerBase
    {
        private readonly ITodoService _service;

        public TodosController(ITodoService service)
        {
            _service = service;
        }

        [HttpGet]
        public ActionResult GetTodos()
        {
            return Ok(_service.GetTodoItems());
        }

        [HttpPut]
        public ActionResult CreateTodo(string itemName)
        {
            var item = _service.AddTodoItem(itemName);

            return Created(nameof(GetTodo), item);
        }

        [HttpGet("id")]
        public ActionResult GetTodo(Guid id)
        {
            return Ok(_service.GetTodoItemById(id));
        }

        [HttpDelete("id")]
        public ActionResult DeleteTodo(Guid id)
        {
            _service.DeleteTodoItem(id);

            return Accepted();
        }

        [HttpPost("id")]
        public ActionResult UpdateTodo(Guid id, int priority)
        {
            _service.UpdateTodoItem(id, priority);

            return Accepted();
        }
    }
}
