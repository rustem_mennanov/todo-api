﻿using System;
using System.Collections.Generic;
using Web.Entities;

namespace Web.Repositories
{
    public interface ITodoRepository
    {
        void AddTodo(TodoItem item);

        void DeleteTodo(Guid id);

        void UpdateTodo(TodoItem item);

        TodoItem GetTodoById(Guid id);

        IEnumerable<TodoItem> GetTodos();
    }
}
