﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Entities;

namespace Web.Repositories
{
    public class TodoRepository : ITodoRepository
    {
        private readonly List<TodoItem> _todos;

        public TodoRepository()
        {
            _todos = new List<TodoItem>();
        }

        public void AddTodo(TodoItem item)
        {
            _todos.Add(item);
        }

        public void DeleteTodo(Guid id)
        {
            _todos.RemoveAll(item => item.Id == id);
        }

        public TodoItem GetTodoById(Guid id)
        {
            return _todos.FirstOrDefault(item => item.Id == id);
        }

        public IEnumerable<TodoItem> GetTodos()
        {
            return _todos
                .OrderBy(item => item.Priority)
                .ToList()
                .AsReadOnly();
        }

        public void UpdateTodo(TodoItem item)
        {
            var todo = _todos.FirstOrDefault(i => i.Id == item.Id);
        }
    }
}
