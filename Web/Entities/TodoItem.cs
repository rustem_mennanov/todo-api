﻿using System;

namespace Web.Entities
{
    public class TodoItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }
    }
}
