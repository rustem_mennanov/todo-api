﻿using System;
using System.Collections.Generic;
using System.Linq;
using Web.Entities;
using Web.Repositories;

namespace Web.Services
{
    public class TodoService : ITodoService
    {
        private ITodoRepository _repository;

        public TodoService(ITodoRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<TodoItem> GetTodoItems()
        {
            return _repository.GetTodos();
        }

        public TodoItem AddTodoItem(string itemName)
        {
            var priority = _repository
                .GetTodos()
                .OrderBy(i => i.Priority)
                .LastOrDefault()?.Priority ?? 0;

            var item = new TodoItem
            {
                Id = Guid.NewGuid(),
                Name = itemName,
                Priority = ++priority
            };

            _repository.AddTodo(item);

            return item;
        }

        public TodoItem GetTodoItemById(Guid id)
        {
            return _repository.GetTodoById(id);
        }

        public void DeleteTodoItem(Guid id)
        {
            _repository.DeleteTodo(id);
        }

        public void UpdateTodoItem(Guid todoId, int priority)
        {
            var todo = _repository.GetTodoById(todoId);

            var max = Math.Max(priority, todo.Priority);
            var min = Math.Min(priority, todo.Priority);

            var moveDown = todo.Priority < priority;
            if (moveDown)
            {
                max++;
                min++;
            }

            var todos = _repository
                .GetTodos()
                .OrderBy(i => i.Priority)
                .SkipWhile(i => i.Priority < min)
                .TakeWhile(i => i.Priority < max);

            foreach (var item in todos)
            {
                item.Priority += moveDown ? -1 : 1;
                _repository.UpdateTodo(item);
            }

            todo.Priority = priority;
            _repository.UpdateTodo(todo);
        }
    }
}
