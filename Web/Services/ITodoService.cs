﻿using System;
using System.Collections.Generic;
using Web.Entities;

namespace Web.Services
{
    public interface ITodoService
    {
        TodoItem AddTodoItem(string itemName);

        IEnumerable<TodoItem> GetTodoItems();

        void DeleteTodoItem(Guid id);

        TodoItem GetTodoItemById(Guid id);

        void UpdateTodoItem(Guid id, int priority);
    }
}
