﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Web.Entities;
using Web.Repositories;
using Web.Services;

namespace Tests
{
    public class TodoServiceTests
    {
        private readonly Mock<ITodoRepository> _mockRepository;

        private readonly ITodoService _sut;
        public TodoServiceTests()
        {
            _mockRepository = new Mock<ITodoRepository>();

            _sut = new TodoService(_mockRepository.Object);
        }

        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void AddTodoItem_ShouldCreateNewTodo_WhenNoTodos()
        {
            // Arrange
            var name = "Name";
            var priority = 1;

            _mockRepository
                .Setup(r => r.GetTodos())
                .Returns(new List<TodoItem>());

            _mockRepository
                .Setup(r => r.AddTodo(It.Is<TodoItem>(
                    item => item.Name == name && item.Priority == priority)))
                .Verifiable();

            // Act
            var result = _sut.AddTodoItem(name);

            // Assert
            Assert.AreEqual(name, result.Name);
            Assert.AreEqual(priority, result.Priority);

            _mockRepository.Verify();
        }

        [Test]
        public void AddTodoItem_ShouldCreateNewTodo_WhenTodosExists()
        {
            // Arrange
            var name = "Name";
            var priority = 2;

            _mockRepository
                .Setup(r => r.GetTodos())
                .Returns(new[] { new TodoItem { Priority = 1 } });

            _mockRepository
                .Setup(r => r.AddTodo(It.Is<TodoItem>(
                    item => item.Name == name && item.Priority == priority)))
                .Verifiable();

            // Act
            var result = _sut.AddTodoItem(name);

            // Assert
            Assert.AreEqual(name, result.Name);
            Assert.AreEqual(priority, result.Priority);

            _mockRepository.Verify();
        }

        [Test]
        public void GetTodoItems_ShouldReturnDataFromSource()
        {
            // Arrange
            var todos = new[]
            {
                new TodoItem(),
                new TodoItem()
            };

            _mockRepository.Setup(r => r.GetTodos()).Returns(todos);

            // Act
            var result = _sut.GetTodoItems();

            // Assert
            Assert.AreSame(todos, result);
        }

        [Test]
        public void GetTodoItemById_ShouldReturnDataFromSource()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var todo = new TodoItem();

            _mockRepository
                .Setup(r => r.GetTodoById(It.Is<Guid>(i => i == guid)))
                .Returns(todo);

            // Act
            var result = _sut.GetTodoItemById(guid);

            // Assert
            Assert.AreSame(todo, result);
        }

        [Test]
        public void DeleteTodoItem_ShouldRemoveItemFromSource()
        {
            // Arrange
            var guid = Guid.NewGuid();

            _mockRepository
                .Setup(r => r.DeleteTodo(It.Is<Guid>(i => i == guid)))
                .Verifiable();

            // Act
            _sut.DeleteTodoItem(guid);

            // Assert
            _mockRepository.Verify();
        }

        [Test]
        public void MoveUpTwice_ShouldUpdateItemsInSource()
        {
            // Arrange
            var firstTodo = new TodoItem { Id = Guid.NewGuid(), Priority = 1 };
            var secondTodo = new TodoItem { Id = Guid.NewGuid(), Priority = 2 };
            var thirdTodo = new TodoItem { Id = Guid.NewGuid(), Priority = 3 };

            _mockRepository
                .Setup(r => r.GetTodoById(thirdTodo.Id))
                .Returns(thirdTodo);

            _mockRepository
                .Setup(r => r.GetTodos())
                .Returns(new[] { firstTodo, secondTodo, thirdTodo });

            _mockRepository
                .Setup(r => r.UpdateTodo(It.IsAny<TodoItem>()));

            // Act
            _sut.UpdateTodoItem(thirdTodo.Id, 1);

            // Assert
            Assert.AreEqual(1, thirdTodo.Priority);
            Assert.AreEqual(2, firstTodo.Priority);
            Assert.AreEqual(3, secondTodo.Priority);
        }

        [Test]
        public void MoveUp_ShouldUpdateItemsInSource()
        {
            // Arrange
            var firstTodo = new TodoItem { Id = Guid.NewGuid(), Priority = 1 };
            var secondTodo = new TodoItem { Id = Guid.NewGuid(), Priority = 2 };
            var thirdTodo = new TodoItem { Id = Guid.NewGuid(), Priority = 3 };

            _mockRepository
                .Setup(r => r.GetTodoById(thirdTodo.Id))
                .Returns(thirdTodo);

            _mockRepository
                .Setup(r => r.GetTodos())
                .Returns(new[] { firstTodo, secondTodo, thirdTodo });

            _mockRepository
                .Setup(r => r.UpdateTodo(It.IsAny<TodoItem>()));

            // Act
            _sut.UpdateTodoItem(thirdTodo.Id, 2);

            // Assert
            Assert.AreEqual(1, firstTodo.Priority);
            Assert.AreEqual(2, thirdTodo.Priority);
            Assert.AreEqual(3, secondTodo.Priority);
        }

        [Test]
        public void MoveDownTwice_ShouldUpdateItemsInSource()
        {
            // Arrange
            var firstTodo = new TodoItem { Id = Guid.NewGuid(), Name = "1", Priority = 1 };
            var secondTodo = new TodoItem { Id = Guid.NewGuid(), Name = "2", Priority = 2 };
            var thirdTodo = new TodoItem { Id = Guid.NewGuid(), Name = "3", Priority = 3 };

            _mockRepository
                .Setup(r => r.GetTodoById(firstTodo.Id))
                .Returns(firstTodo);

            _mockRepository
                .Setup(r => r.GetTodos())
                .Returns(new[] { firstTodo, secondTodo, thirdTodo });

            _mockRepository
                .Setup(r => r.UpdateTodo(It.IsAny<TodoItem>()));

            // Act
            _sut.UpdateTodoItem(firstTodo.Id, 3);

            // Assert
            Assert.AreEqual(1, secondTodo.Priority);
            Assert.AreEqual(2, thirdTodo.Priority);
            Assert.AreEqual(3, firstTodo.Priority);
        }
        [Test]
        public void MoveDown_ShouldUpdateItemsInSource()
        {
            // Arrange
            var firstTodo = new TodoItem { Id = Guid.NewGuid(), Name = "1", Priority = 1 };
            var secondTodo = new TodoItem { Id = Guid.NewGuid(), Name = "2", Priority = 2 };
            var thirdTodo = new TodoItem { Id = Guid.NewGuid(), Name = "3", Priority = 3 };

            _mockRepository
                .Setup(r => r.GetTodoById(firstTodo.Id))
                .Returns(firstTodo);

            _mockRepository
                .Setup(r => r.GetTodos())
                .Returns(new[] { firstTodo, secondTodo, thirdTodo });

            _mockRepository
                .Setup(r => r.UpdateTodo(It.IsAny<TodoItem>()));

            // Act
            _sut.UpdateTodoItem(firstTodo.Id, 2);

            // Assert
            Assert.AreEqual(1, secondTodo.Priority);
            Assert.AreEqual(2, firstTodo.Priority);
            Assert.AreEqual(3, thirdTodo.Priority);
        }
    }
}
